<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Role;
use Illuminate\Support\Arr;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function gamer(){
        return $this->hasOne('App\Models\Gamer','mail','email');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
    }

    public function isEmployee()
    {
        $roles = $this->roles->toArray();
        return !empty($roles);
    }

    public function hasRole($check)
    {
//        dd($this->roles->toArray());
        $arr=in_array($check, Arr::pluck($this->roles->toArray(),'name_role'));
//        dd($arr);
        return $arr;
//        return in_array($check, array_pluck($this->roles->toArray(), 'name'));
    }

    public function getIdInArray($array, $term)
    {
        foreach ($array as $key => $value) {
            if ($value == $term) {
                return $key + 1;
            }
        }
        return false;
    }

    public function getEmployee($title){
        $assig_roles=array();
        $roles=array_pluck(Role::all()->toArray(),'name');
        switch ($title){
            case 'admin':
                $assig_roles[]=$this->getIdInArray($roles,'admin');
            case 'client':
                $assig_roles[]=$this->getIdInArray($roles,'client');
                break;
            default:
                $assig_roles[]=false;
        }
        $this->roles()->attach($assig_roles);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
