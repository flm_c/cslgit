<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gamer extends Model
{
    protected $table = 'gamers';

    protected $fillable = [
        'fio',
        'birth',
        'nickname',
        'sizeCloth',
        'telephone',
        'mail',
        'curs',
        'team_id',
        'university_id',
        'faculty_id',
        'game_id',
    ];

    public function faculty()
    {
        return $this->belongsTo('App\Models\Faculty');
    }

    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'email', 'mail');
    }

    public function game()
    {
        return $this->belongsTo('App\Models\Game');
    }

    public function university()
    {
        return $this->belongsTo('App\Models\University');
    }
}
