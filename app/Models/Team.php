<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    protected $fillable = [
        'name_team',
    ];

    public function gamers()
    {
        return $this->hasMany('App\Models\Gamer');
    }

}
