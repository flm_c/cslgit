<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table='games';
//    protected $fillable=[
//        'headline',
//        'shortret',
//        'body',
//        'image'
//    ];

    public function gamers(){
        return $this->hasMany('App\Models\Gamer');
    }
}
