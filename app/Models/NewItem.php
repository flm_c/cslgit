<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewItem extends Model
{
    protected $table='news';
    protected $fillable=[
        'headline',
        'shortret',
        'body',
        'image'
    ];
}
