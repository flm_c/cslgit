<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $table='universities';

    public function gamers(){
        return $this->hasMany('App\Models\Gamer');
    }
}
