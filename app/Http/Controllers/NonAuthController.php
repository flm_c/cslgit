<?php

namespace App\Http\Controllers;

use App\Item;
use App\User;
use Illuminate\Http\Request;
use App\Models\NewItem;
use App\Models\Team;
use App\Models\University;
use App\Models\Game;
use App\Models\Gamer;
use Illuminate\Support\Facades\Auth;
use App\Role;

class NonAuthController extends Controller
{
    public function home()
    {
        return view('site.home', array(
            'title' => 'Главная',
        ));
    }

    public function news()
    {
        $news = array();
        $pages = NewItem::orderBy('id', 'desc')->paginate(10);
        foreach ($pages as $page) {
            $item = array(
                'id' => $page->id,
                'headline' => $page->headline,
                'shortret' => $page->shortret,
                'body' => $page->body,
                'image' => $page->image,
            );
            array_push($news, $item);
        }
        $data = [
            'pages' => $pages,
        ];
        return view('site.news', array(
            'news' => $news,
            'title' => 'Новости',
        ),
            $data
        );

    }

    public function teams() //дописать
    {

//        $gamer=Gamer::find(1);
//        $team=$gamer->university->name_university;
//        dd($team);
//        $team=Team::find(1);
//        $gamers=$team->gamers;
//        dd($gamers);
//        $games=Game::select('name_game')->get();
//        dd($games->where('name_game','Dota 2'));
//        dd($games);

//        $team=Team::where('name_team','Пираты');
//        $gamer=Gamer::find(6);
//        $teams=$gamer->team->name_team;
//        dd($teams);
        $games=Game::all();
        $teams=Team::whereNotNull('game')->get();
        foreach ($teams as $item){
            $gamers=$item->gamers;
        }

        return view('site.teams',array(
            'teams'=>$teams,
            'gamers'=>$gamers,
            'games'=>$games,
            'title'=>'Команды',
        ));
    }

    public function item($id)
    {
        if (!$id) abort(404);
        if(view()->exists('site.page')){
            $pages=NewItem::where('id',strip_tags($id))->first();
            $data=array();
            $array=array('page'=>$pages);
            array_push($data,$array);
            return view('site.page',array(
                'data'=>$data,
                'title'=>$pages->headline,
            ));
        }
        else abort(404);
    }

    public function about()
    {
        return view('site.about',array(
            'title'=>'О нас',
        ));
    }
}
