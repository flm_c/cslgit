<?php

namespace App\Http\Controllers;

use App\Models\Gamer;
use Illuminate\Http\Request;
use App\Models\NewItem;
use App\Models\Team;
use App\Models\Game;
use App\User;
use Validator;


class ClientController extends Controller
{
    public function gamers(){
        if (view()->exists('client.gamers')) {
            $gamers = Gamer::orderBy('game_id')->get();
            $data = [
                'title' => 'База игроков',
                'gamers' => $gamers,
            ];
            return view('client.gamers', $data);
        }
        abort(404);
    }

    public function profile(Gamer $gamer,Request $request){
        if ($request->isMethod('post')){
            $input=$request->except('_token');

            //Свой валидатор
            $validator=Validator::make($input,[
//                'nickname' => 'required|max:100',
                'sizeCloth' => 'max:10',
                'telephone' => 'max:12',
                'curs' => 'required',
            ]);
//если валитарор фэйлит, то отправляет на стр ошибку с проблемой
            if($validator->fails()){
                return redirect()->route('profile',['gamer'=>$gamer->id])->withErrors($validator)->withInput();

            }

            $gamer->fill($input);
            //что новость обновлена
            if ($gamer->update()) {
                return redirect('/');
            }
        }


        $old_gamer = $gamer;
        if (view()->exists('client.profile')) {
            $data = [
                'title' => 'Редактирование информации об игроке',
                'data' => $old_gamer,
            ];
            return view('client.profile', $data);
        };



    }
}
