<?php

namespace App\Http\Controllers;

use App\Models\Gamer;
use Illuminate\Http\Request;
use App\Models\NewItem;
use App\Models\Team;
use App\Models\Game;

use Validator;

class AdminController extends Controller
{
    public function news()
    {
        if (view()->exists('admin.news')) {

            $news = NewItem::orderBy('id', 'desc')->get();
            $data = [
                'title' => 'Статьи',
                'news' => $news,
            ];
            return view('admin.news', $data);
        }
        abort(404);
    }

    public function newsAdd(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->except('_token');

            $validator = Validator::make($input, [
                'headline' => 'required|max:100',
                'shortret' => 'required|max:250',
                'body' => 'required',
                'image' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect()->route('newsAdd')->withErrors($validator)->withInput();
            }
            $img = $request->file('image');
            $input['image'] = $img->getClientOriginalName();
            $img->move(public_path() . '/assets/img', $input['image']);


            $news = new NewItem();
            $news->fill($input);

            if ($news->save()) {
                return redirect('admin')->with('status', 'Новость добавлена');
            }
        }
        if (view()->exists('admin.news_add')) {
            $data = [
                'title' => 'Новая статья',
            ];
            return view('admin.news_add', $data);
        }
        abort(404);
    }

    public function newsEdit(NewItem $item, Request $request)
    {
        if($request->isMethod('delete')){
            $item->delete();
            return redirect('admin')->with('status','Страница удалена');
        }

        //Тут POST
        //Если запрос обращ-пост- запускается данный иф
        if ($request->isMethod('post')){
            //интересует вся инфа, кроме случайного токен ключа
            $input=$request->except('_token');
//            $img = $request->file('image');
//            $input['image'] = $img->getClientOriginalName();
            //Свой валидатор
            $validator=Validator::make($input,[
                'headline' => 'required|max:100',
                'shortret' => 'required|max:250',
                'body' => 'required',
            ]);
//если валитарор фэйлит, то отправляет на стр ошибку с проблемой
            if($validator->fails()){
                return redirect()->route('newsEdit',['item'=>$input['id']])->withErrors($validator)->withInput();
            }
//            загружается ли файл на серв
            if($request->hasFile('image')){
                $img = $request->file('image');
                //Предоставляет путь к каталогу паблик
                $img->move(public_path() . '/assets/img', $img->getClientOriginalName());
                $input['image']=$img->getClientOriginalName();
            }
            else{
                $input['image']=$input['old_image'];
            }
            //Удаляем лишнюю old_image
            unset($input['old_image']);

            $item->fill($input);
            //Оповещение, что новость обновлена
            if ($item->update()) {
                return redirect('admin')->with('status', 'Статья обновлена');
            }
        }

        //Тут GET
        $old=$item->toArray();
        if(view()->exists('admin.news_edit')){
            $data=[
                'title'=>'Редактирование статьи '.$old['headline'],
                'data'=>$old,
            ];
            return view('admin.news_edit',$data);
        };
    }

    public function gamers()
    {
        if (view()->exists('admin.gamers')) {
            $gamers = Gamer::orderBy('game_id', 'desc')->get();
            $data = [
                'title' => 'Игроки',
                'gamers' => $gamers,
            ];
            return view('admin.gamers', $data);
        }
        abort(404);
    }


    public function gamersEdit(Gamer $player, Request $request){

        if($request->isMethod('delete')){
            $player->delete();
            return redirect('admin')->with('status','Игрок удален');
        }

        //Тут POST
        //Если запрос обращ-пост- запускается данный иф
        if ($request->isMethod('post')) {
            //интересует вся инфа, кроме случайного токен ключа
            $input = $request->except('_token');

            //Свой валидатор
            $validator = Validator::make($input, [
                'fio' => 'required|max:255',
                'nickname' => 'required|max:255',
                'telephone' => 'required|max:12',
                'curs' => 'required',

            ]);
//если валитарор фэйлит, то отправляет на стр ошибку с проблемой
            if ($validator->fails()) {
                return redirect()->route('gamersEdit', ['player' => $input['id']])->withErrors($validator)->withInput();
            }
            $player->fill($input);
            //Оповещение, что новость обновлена
            if ($player->update()) {
                return redirect()->route('gamers')->with('status', 'Информация об игроке '.$player['nickname'].' обновлена');
            }
        }

        //Тут GET
        $old = $player;
        if (view()->exists('admin.gamers_edit')) {
            $data = [
                'title' => 'Редактирование информации об игроке',
                'data' => $old,
            ];
            return view('admin.gamers_edit', $data);
        };
    }


    public function bestTeams(){
        $games=Game::all();
        $teams=Team::all();
        $data=[
            'title'=>'Сборные ТюмГУ',
            'games'=>$games,
            'teams'=>$teams,
        ];
        return view('admin.teams',$data);
    }

    public function teamsEdit(Game $game,Request $request){
        $old = $game;
        $game=$old->name_game;
        if (view()->exists('admin.news_edit')) {
            $data = [
                'title' => 'Редактирование информации об игроке',
                'data' => $old,
            ];
            return view('admin.gamers_edit', $data);
        };
    }
}
