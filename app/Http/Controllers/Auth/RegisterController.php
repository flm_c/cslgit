<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Gamer;
use App\Models\University;
use App\Models\Faculty;
use App\Models\Team;
use App\Models\Game;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $uni=University::find(9);
        $faculty=Faculty::find(3);
        $team=Team::find(5);
        $game=Game::find(11);

        $user_cr=User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        $gamer_create=Gamer::create([
            'fio'=>'Неизвестный',
            'nickname' => $data['name'],
            'birth'=>'1999-01-01',
            'mail' => $data['email'],
            'telephone'=>'1',
            'university_id'=>$uni->id,
            'faculty_id'=>$faculty->id,
            'team_id'=>$team->id,
            'game_id'=>$game->id,
            'curs'=>'1',
        ]);
        return $user_cr;
    }
}
