@extends('layouts.client')

@section('header')
    @include('client.header')
@endsection

@section('content')
    @include('client.content_gamers')
@endsection