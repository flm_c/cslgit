<div class="container">
    {{--@if($gamersDotaReserve)--}}
    <div class="container mb-4 pt-4">
        {!! Form::label('game','Все игроки',['class'=>'col-xs-2 control-label h1']) !!}
    </div>
    <table class="table table-sm table-hover table-striped table-dark ">
        <thead>
        <tr>
            <th>Игра</th>
            <th>ФИО</th>
            <th>Ник</th>
            <th>Команда</th>
            <th>Дата рождения</th>
            <th>Университет</th>
        </tr>
        </thead>
        <tbody>
        @foreach($gamers as $player)
            <tr>
                <td>{{$player->game->name_game}}</td>
                <td>{{$player->fio}}</td>
                <td>{{$player->nickname}}</td>
                <td>{{$player->team->name_team}}</td>
                <td>{{date('d-m-Y', strtotime($player->birth))}}</td>
                <td>{{$player->university->name_university}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
    {{--@endif--}}


</div>
