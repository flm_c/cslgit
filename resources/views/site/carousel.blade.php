<div id="carouselExampleControls" class="carousel slide col-lg-10 centered container magrin-top-2em" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="assets/carouselimg/first.jpg" alt="Первый слайд">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="assets/carouselimg/second.jpg" alt="Второй слайд">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="assets/carouselimg/third.jpg" alt="Третий слайд">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>