{{--Надо делать чет с таблицой <table>--}}
{{--Здесь только основные составы, без резевов. те. 5--}}
<div class="container">
    <div class="container">
        @foreach($teams as $team)
            <div class="container col-11">
                <h2 class="pt-3 pb-2">{{$team->game}} - {{$team->name_team}}</h2>
                <table class="table table-hover table-dark text-center">
                    <thead>
                    <tr>
                        <th>ФИО</th>
                        <th>Ник</th>
                        <th>Дата рождения</th>
                        <th>Институт</th>
                    </tr>
                    </thead>
                    @foreach($games as $game)
                        @if($game->name_game==$team->game)
                            @foreach($gamers as $gamer)
                                <tr>
                                    <td>{{$gamer->fio}}</td>
                                    <td>{{$gamer->nickname}}</td>
                                    <td>{{date('d-m-Y', strtotime($gamer->birth))}}</td>
                                    <td>{{$gamer->university->name_university}}</td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                </table>
            </div>
        @endforeach
    </div>
</div>