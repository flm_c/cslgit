
@foreach($news as $page)
    <div class="container pl-5 pr-5 pb-3 pt-3">
        <div class="container border border-dark border-bottom-1">
            <div class="row">
                {{--width="230" height="150"--}}
                <div class="col-xl-3 col-lg-4 col-md-5 col-sm-12 mt-3 mb-3"><img src="assets/img/{{$page['image']}}" width="240" height="150" alt=""></div>
                <div class="row col-xl-9 col-9 col-lg-8 col-md-7 col-sm-12  mt-2 mb-2 pl-5">
                    <table class="text-light" cellspacing="1">
                        <tr>
                            <th scope="col"><a href="/news/{{$page['id']}}" class="aWhite">{{$page['headline']}}</a></th>
                        </tr>
                        <tr>
                            <td>
                                <p>{{$page['shortret']}}</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endforeach
<div class="container pl-5 pt-3 pb-3 bg-content">{{$pages->links()}}</div>
