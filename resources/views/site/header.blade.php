<nav class="navbar navbar-expand-lg navbar-dark bg-header">
    <a class="navbar-brand" href="/">
        <img src="{{asset('assets/img/logo.png')}}" width="55" height="55" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
            aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="col-lg-1"></div>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0 ">
            <li class="nav-item">
                <a class="nav-link" href="/">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/news">Новости</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="/teams">Команды</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="/about">О нас</a>
            </li>
            @if(Auth::check())
                <li class="nav-item">
                    <a class="nav-link " href="/gamers">Игроки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="/profile/{{Auth::user()->gamer->id}}">Профиль</a>
                </li>

            @endif
        </ul>
    </div>
    <ul class="navbar-nav ml-auto">
    {{--типо правая часть--}}
    <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdown">

                    {{--@if(Auth::user()->role=="admin")--}}
                    {{--С этим чет придумать--}}
                    <a class="dropdown-item t text-dark" href="/admin">Админ.панель</a>
                    {{--@endif--}}

                    <a class="dropdown-item t adark text-dark" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </div>
            </li>
        @endguest
    </ul>

</nav>

{{--@foreach($menu as $tab)--}}
{{--<li class="nav-item">--}}
{{--<a href="{{$tab['alias']}}" class="nav-link active">{{$tab['title']}}</a>--}}
{{--</li>--}}

{{--@endforeach--}}
