
@foreach($data as $array)
    <div class="container">
        <h1 class="col-xl-12 pt-4">{{$array['page']->headline}}</h1>
        <div class="container pt-3">
            <h4>{{$array['page']->shortret}}</h4>
        </div>
        <div class="container pt-4">
            {!! $array['page']->body !!}
        </div>
    </div>

@endforeach
