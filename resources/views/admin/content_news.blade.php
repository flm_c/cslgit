
<div class="container">
    @if($news)
        <div class="container mb-4 mt-4">
            <button type="button" class="btn btn-success">
                {!! Html::link(route('newsAdd'),'Новая статья') !!}
            </button>
        </div>
        <table class="table table-hover table-striped table-dark ">
            <thead>
            <tr class="text-center">
                <th>№</th>
                <th>Заголовок</th>
                <th>Кратк.описание</th>
                <th>Содержание</th>
                <th>Удалить</th>
            </tr>
            </thead>
            <tbody>


            @foreach($news as $i=>$item)
                <tr>
                    <td class="text-center">{{$item->id}}</td>
                    <td class="text-center"><a href="/admin/news/edit/{{$item->id}}" class="text-light">{{$item->headline}}</a></td>
                    <td class="text-left">{{$item->shortret}}</td>
                    <td class="text-justify">{{$item->body}}</td>
                    <td>
                        {!! Form::open(['url'=>route('newsEdit',['item'=>$item->id]), 'class'=>'form-horizontal','method'=>'DELETE']) !!}
                        {{--Создается <input type="hidden" name="_method" value="DELETE">--}}
                        {{method_field("DELETE")}}
                        {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit']) !!}

                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif


</div>
