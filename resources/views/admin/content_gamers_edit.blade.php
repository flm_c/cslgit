<div class="wrapper container">

    {!! Form::open(['url'=>route('gamersEdit',array('player'=>$data['id'])),'class'=>'form-horizontal','method'=>'POST']) !!}
    <div class="form-group pt-4 h1">
        {!! Form::hidden('id',$data['id']) !!}
        {!! Form::label('game_id','Игра: '.$data->game->name_game,['class'=>'col-xs-2 control-label']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('fio','ФИО',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('fio',$data['fio'],['class'=>'form-control','placeholder'=>'Введите фио игрока'])!!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('nickname','Игровой ник',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('nickname', $data['nickname'],['class'=>'form-control','placeholder'=>'Введите ник игрока']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('team_id','Команда',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('team_id', $data->team->name_team,['class'=>'form-control','placeholder'=>'Введите команду игрока']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('game_id','Игра',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('gama_id', $data->game->name_game,['class'=>'form-control','placeholder'=>'Введите игру']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('birth','День рождения',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::date('birth', $data['birth'],['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('university_id','Институт',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('university_id', $data->university->name_university,['class'=>'form-control','placeholder'=>'Введите институт игрока']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('curs','Курс',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('curs', $data['curs'],['class'=>'form-control','placeholder'=>'Введите курс игрока']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('faculty_id','Направление',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('faculty_id', $data->faculty->name_faculty,['class'=>'form-control','placeholder'=>'Введите учебное направление игрока']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('telephone','Телефон',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('telephone', $data['telephone'],['class'=>'form-control','placeholder'=>'Введите телефон игрока']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('sizeCloth','Размер одежды',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('sizeCloth', $data['sizeCloth'],['class'=>'form-control','placeholder'=>'Введите размер одежды игрока']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('Сохранить',['class'=>'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>

    {!!  Form::close() !!}
</div>