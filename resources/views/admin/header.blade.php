<nav class="navbar navbar-expand-lg navbar-dark bg-header">
    <a class="navbar-brand" href="/admin">
        <h3>Админ.панель</h3>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
            aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="col-lg-1"></div>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0 ">
            <li class="nav-item">
                <a class="nav-link" href="{{route('news')}}">Новости</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('gamers')}}">Игроки</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('bestTeams')}}">Сборные Тюмгу</a>
            </li>
        </ul>
    </div>
    <div class="container col-1">
        {{--<a class="" href="{{route('logout')}}">--}}
        {{--Выйти;--}}
        {{--</a>--}}
    </div>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdown">
                <a class="dropdown-item t adark text-dark" href="/">
                   Выйти из адм.панели
                </a>
                <a class="dropdown-item t adark text-dark" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Выйти из уч.зап.') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </div>
        </li>
    </ul>
</nav>