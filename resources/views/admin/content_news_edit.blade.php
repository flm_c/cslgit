<div class="wrapper container-fluid">

    {!! Form::open(['url'=>route('newsEdit',array('item'=>$data['id'])),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}
    <div class="form-group">
        {!! Form::hidden('id',$data['id']) !!}
        {!! Form::label('headline','Заголовок',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('headline',$data['headline'],['class'=>'form-control','placeholder'=>'Введите заголовок статьи']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('shortret','Краткое описание',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('shortret',$data['shortret'],['class'=>'form-control','placeholder'=>'Введите краткую информацию о статье'])!!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('body','Основной текст',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::textarea('body', $data['body'],['id'=>'editor','class'=>'form-control','placeholder'=>'Введите текст статьи']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('old_image','Старое изображение:',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-offset-2 col-xs-10">
            {!! Html::image('assets/img/'.$data["image"],'',['class'=>'col-xs-2 control-label','width'=>"460", 'height'=>"300"]) !!}
            {!! Form::hidden('old_image',$data['image']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('image','Новое изображение (Выберете, если нужно)',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::file('image',['data-buttonText'=>'Выберите изображение','data-buttonName'=>'btn-primary','data-placeholder'=>'Файла нет']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('Сохранить',['class'=>'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>


    {!!  Form::close() !!}

    <script>
        CKEDITOR.replace('editor')
    </script>

</div>