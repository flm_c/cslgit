<div class="wrapper container-fluid">

    {!! Form::open(['url'=>route('newsAdd'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}
    <div class="form-group">
        {!! Form::label('headline','Заголовок',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('headline',old('headline'),['class'=>'form-control','placeholder'=>'Введите заголовок статьи']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('shortret','Краткое описание',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::text('shortret',old('shortret'),['class'=>'form-control','placeholder'=>'Введите краткую информацию о статье'])!!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('body','Основной текст',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::textarea('body',old('body'),['id'=>'editor','class'=>'form-control','placeholder'=>'Введите текст статьи']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('image','Изображение',['class'=>'col-xs-2 control-label']) !!}
        <div class="col-xs-8">
            {!! Form::file('image',['data-buttonText'=>'Выберите изображение','data-buttonName'=>'btn-primary','data-placeholder'=>'Файла нет']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-offset-2 col-xs-10">
            {!! Form::button('Сохранить',['class'=>'btn btn-primary','type'=>'submit']) !!}
        </div>
    </div>


    {!! Form::close() !!}

    <script>
        CKEDITOR.replace('editor')
    </script>

</div>