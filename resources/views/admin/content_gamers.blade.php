
<div class="container">
    {{--@if($gamersDotaReserve)--}}
        <div class="container mb-4 mt-4">
            {!! Form::label('game','Игроки',['class'=>'col-xs-2 control-label h1']) !!}
        </div>
        <table class="table table-sm table-hover table-striped table-dark ">
            <thead>
            <tr>
                <th>Игра</th>
                <th>ФИО</th>
                <th>Ник</th>
                <th>Команда</th>
                <th>Дата рождения</th>
                <th>Университет</th>
                <th>Удалить</th>>
            </tr>
            </thead>
            <tbody>
            @foreach($gamers as $player)
                <tr>
                    <td>{{$player->game->name_game}}</td>
                    <td><a href="/admin/gamers/edit/{{$player->id}}" class="text-light">{{$player->fio}}</a></td>
                    <td>{{$player->nickname}}</td>
                    <td>{{$player->team->name_team}}</td>
                    <td>{{date('d-m-Y', strtotime($player->birth))}}</td>
                    <td>{{$player->university->name_university}}</td>
                    <td>
                        {!! Form::open(['url'=>route('gamersEdit',['player'=>$player->id]), 'class'=>'form-horizontal','method'=>'DELETE']) !!}
                        {{--Создается <input type="hidden" name="_method" value="DELETE">--}}
                        {{method_field("DELETE")}}
                        {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit']) !!}

                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    {{--@endif--}}


</div>
