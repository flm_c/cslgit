<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NotNullInGamers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gamers', function (Blueprint $table) {
            $table->foreign('university_id')->default(1)->change();
            $table->foreign('game_id')->default(1)->change();
            $table->foreign('team_id')->default(1)->change();
            $table->foreign('faculty_id')->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gamers', function (Blueprint $table) {
            $table->foreign('university_id')->default(0)->change();
            $table->foreign('game_id')->default(0)->change();
            $table->foreign('team_id')->default(0)->change();
            $table->foreign('faculty_id')->default(0)->change();
        });
    }
}
