<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTablesId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gamers', function (Blueprint $table) {
            $table->dropForeign('gamers_faculty_name_foreign');
            $table->dropColumn('faculty_name');
        });

        Schema::drop('faculties');

        Schema::create('faculties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_faculty');
            $table->bigInteger('university_id')->unsigned();

        });

        Schema::table('faculties',function (Blueprint $table){
            $table->foreign('university_id')->references('id_university')->on('universities')->onDelete('cascade');
        });

        Schema::table('gamers', function (Blueprint $table) {
            $table->renameColumn('id_gamer', 'id');
            $table->Biginteger('faculty_id')->unsigned()->after('university_id');
            $table->foreign('faculty_id')->references('id')->on('faculties')->onDelete('cascade');
        });

        Schema::table('games', function (Blueprint $table) {
            $table->renameColumn('id_game', 'id');
        });

        Schema::table('teams', function (Blueprint $table) {
            $table->renameColumn('id_team', 'id');
        });

        Schema::table('universities', function (Blueprint $table) {
            $table->renameColumn('id_university', 'id');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
