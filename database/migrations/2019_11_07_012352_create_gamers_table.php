<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gamers', function (Blueprint $table) {
            $table->bigIncrements('id_gamer');
            $table->string('fio');
            $table->date('birth');
            $table->string('nickname');
            $table->string('sizeCloth');
            $table->string('telephone', 12);

            $table->string('mail');
//            $table->foreign('mail')->references('email')->on('users')->onDelete('cascade');

            $table->bigInteger('university_id')->unsigned();
//            $table->foreign('university_id')->references('id_university')->on('universities')->onDelete('cascade');

            $table->string('faculty_name');
//            $table->foreign('faculty_name')->references('name_faculty')->on('faculties')->onDelete('cascade');

            $table->integer('curs');
//            $table->foreign('curs')->references('name_faculty')->on('faculties')->onDelete('cascade');

            $table->Biginteger('team_id')->unsigned();
//            $table->foreign('team_id')->references('id_team')->on('teams')->onDelete('cascade');

            $table->Biginteger('game_id')->unsigned();
//            $table->foreign('game_id')->references('game_id')->on('games')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::table('gamers', function (Blueprint $table) {
            $table->foreign('mail')->references('email')->on('users')->onDelete('cascade');
            $table->foreign('university_id')->references('id_university')->on('universities')->onDelete('cascade');
            $table->foreign('faculty_name')->references('name_faculty')->on('faculties')->onDelete('cascade');
            $table->foreign('team_id')->references('id_team')->on('teams')->onDelete('cascade');
            $table->foreign('game_id')->references('id_game')->on('games')->onDelete('cascade');
//            $table->foreign('curs')->references('kurs')->on('faculties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gamers');
    }
}
