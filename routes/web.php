<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::group(['middleware' => 'web'], function () {

    Route::match(['get','post'],'/', ['uses'=>'NonAuthController@home','as'=>'homes']);
    Route::match(['get','post'],'/news', ['uses'=>'NonAuthController@news','as'=>'news']);
    Route::match(['get','post'],'/teams', ['uses'=>'NonAuthController@teams','as'=>'teams']);
    Route::match(['get','post'],'/news/{alias}', ['uses'=>'NonAuthController@item','as'=>'item']);
    Route::match(['get','post'],'/about', ['uses'=>'NonAuthController@about','as'=>'about']);

});



Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix'=>'admin','middleware' => 'role:admin'], function () {
        Route::get('/',['as'=>'admin',function (){
           if(view()->exists('admin.index')){
               $data=['title'=>'Админ. панель'];
               return view('admin.index',$data);
           }
        }]);

        Route::group(['prefix' => 'news'], function () {
            //admin/pages/
            Route::get('/', ['uses' => 'AdminController@news', 'as' => 'news']);
            //добавление новости admin/news/add
            Route::match(['get', 'post'], '/add', ['uses' => 'AdminController@newsAdd', 'as' => 'newsAdd']);
            //изменение стр admin/pages/edit/1
            Route::match(['get', 'post', 'delete'], '/edit/{item}', ['uses' => 'AdminController@newsEdit', 'as' => 'newsEdit']);
        });
        Route::group(['prefix' => 'gamers'], function () {
            Route::get('/', ['uses' => 'AdminController@gamers', 'as' => 'gamers']);
            //Все игроки с доп информацией по нажатию
            Route::match(['get', 'post', 'delete'], '/edit/{player}', ['uses' => 'AdminController@gamersEdit', 'as' => 'gamersEdit']);
            //изменение стр admin/pages/edit/1
        });
        Route::group(['prefix'=>'teams'], function (){
           Route::match(['get'],'/',['uses'=>'AdminController@bestTeams','as'=>'bestTeams']);
           Route::match(['get','post'],'/{game}',['uses'=>'AdminController@teamsEdit','as'=>'teamsEdit']);
        });



    }); //конец admin


    Route::get('/gamers',['uses'=>'ClientController@gamers','as'=>'seeGamers']);
    Route::match(['get', 'post'], '/profile/{gamer}', ['uses' => 'ClientController@profile', 'as' => 'profile']);


});//тут васе авторизованные

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
?>